
public class PortableDevice {
	String age;//global variable
	public static void main(String[] args) {
		//binary literal
		int age = 0b1010;//10
		System.out.println(age);
		//octal literal
		int gives = 010;
		System.out.println(gives);
		/*
		 * 10= 0 * 8 ^ 0 + 1 * 8 ^ 1
		 * 
		 * */
		byte hexvariable = 0x12;
		System.out.println(hexvariable);
		
		int sum = 99;
		
		float average= 5_67.5_6f;
		float median = 456456789987656L;
		int credit = 56_78;
		long mobile_no = 7987_654_312L;
		
		char c = 97;
		char t = '\u03A9';
		char y = 'A';
		
		System.out.println(c);
		System.out.println(t);
		
		String s = "\u03A9 is a character";
		System.out.println(s);
		
		String k = "Welcome \n" + " to \t" + " the Milkyway Galaxy";
		System.out.println(k);
		
		
		
	}
}
