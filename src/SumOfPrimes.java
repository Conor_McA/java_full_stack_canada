
public class SumOfPrimes {

	static boolean isPrime(int n) {
		int factor = 0;
		for(int i=1; i<=n;i++) {
			if (n%i == 0) {
				factor++;
			}
		}
		if (factor==2) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		int n = 10;
		for(int i=2; i<=n;i++) {
			if(isPrime(i) && isPrime(n-i)) {
				System.out.println(n + " = " + i + " + " + (n-i));
			}
		}
	}
}
