
public class LocalInnerClass {
// A class that is created inside a method is called as local inner class
// They belong to the block inside which it is created
// They can have final or abstract access modifiers too

	void display() {
		class Laptop{
			static int testscore;
			String name;
			
			static void calculate() {
				System.out.println("Calcualted");
			}
			void show() {
				System.out.println(testscore);
				System.out.println(name);
			}
			
		}
		Laptop l = new Laptop();
		l.show();
		Laptop.calculate();
	}
}
