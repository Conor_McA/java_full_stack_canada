
public class TestNested {
public static void main(String[] args) {
	MemberInnerClass mic = new MemberInnerClass();
	mic.show();
	MemberInnerClass.display();
	MemberInnerClass.InnerClass ic = mic.new InnerClass();
	ic.show();
	MemberInnerClass.InnerClass.display();
	
	
	LocalInnerClass lic = new LocalInnerClass();
	lic.display();
	
	StaticInnerClass.StInner.show();
	StaticInnerClass.StInner sin= new StaticInnerClass.StInner();
	sin.check();
	
	
}
}
