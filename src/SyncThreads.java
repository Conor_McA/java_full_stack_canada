
public class SyncThreads extends Thread{

	@Override
	public void run() {
		DotMatrixPrinter.printNumbers(0);
	}
}

class SyncThreads1 extends Thread{
	
	@Override
	public void run() {
		DotMatrixPrinter.printNumbers(10);
	}
}

class SyncThreads2 extends Thread{
	
	@Override
	public void run() {
		DotMatrixPrinter.printNumbers(4);
	}
}

class SyncThreads3 extends Thread{
	
	@Override
	public void run() {
		DotMatrixPrinter.printNumbers(100);
	}
}
