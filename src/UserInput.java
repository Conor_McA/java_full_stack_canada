import java.util.Scanner;

public class UserInput {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the label");
		String label= s.next();
		System.out.println("Enter the name");
		s.nextLine();
		String name = s.nextLine();
		System.out.println("Enter the character");
		char c = s.next().charAt(0);
		//byte = nextByte();
		//short = nextShort();
		//int = nextInt();
		//long = nextLong();
		//float = nextFloat();
		//double = nextDouble();
		//boolean = nextBoolean();
		//String (one word) = next();
		//String (multiple words / sentences) = nextLine();
		//char = next().charAt(0);

	}
}


// An object in java can be created using 5 different ways
// The most commonly used on is using new keyword
// new keyword is followed by constructor of  the class
// with array as the exception

// Every class comes with an automatic constructor when created
// That automatic constructor is called as default constructor

