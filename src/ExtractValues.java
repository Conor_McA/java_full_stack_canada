import java.time.LocalDate;
import java.time.Period;

public class ExtractValues {
	public static void main(String[] args) {
		LocalDate date = LocalDate.now();
		System.out.println(date.getDayOfMonth());
		System.out.println(date.getDayOfYear());
		System.out.println(date.getMonth());
		System.out.println(date.getMonthValue());
		System.out.println(date.getYear());
		
		LocalDate mybday = LocalDate.of(1984,5,16);
		System.out.println(mybday);
		Period timeperiod = Period.between(mybday,date);
		System.out.println(timeperiod.getYears()+" years "+timeperiod.getMonths()+" months "+
							timeperiod.getDays()+" days");
	}
}
