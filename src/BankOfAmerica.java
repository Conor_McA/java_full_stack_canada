
public class BankOfAmerica {
	public static void main(String[] args) {
		Account acc = new Account();
		AccountHolder account_holder = new AccountHolder(acc);
		Thread t1= new Thread(account_holder);
		Thread t2= new Thread(account_holder);
		t1.setName("Shiraz");
		t2.setName("Conor");
		t1.start();
		t2.start();
	}
}
