
public class AssignmentOprators {
	public static void main(String[] args) {
		int a = 10;
		a += 100; // a = a + 100
		
		//relational
		
		// == with respect to a non-primitive data type will compare the address
		
		PortableDevice p = new PortableDevice();
		PortableDevice t = new PortableDevice();
		System.out.println(p==t);
		
		String jamesbond = "007";
		String agent = "007";
		
		String jb = new String("007");
		String jc = new String("007");
		
		System.out.println(jamesbond == agent);
		System.out.println(jb==jc);
		
		//Logical Operator (a>b) && (b>c) || !(booleanexpr)
		
		//Bitwise Operator & | ^ ~ << >>
		
		//~10 = 10 + 1 = -11    -23 + 1 = +22
		
		System.out.println(~10);
		System.out.println(~-23);
		
		//Conditional Operator
		
		// (expression)?(truepart) : (falsepart) alternative for if-selse
		
		int b = 122;
		int k = 234;
		System.out.println(b+k+"Hello");// 356Hello
		System.out.println("Hello"+(b+k));//
		System.out.println("Hello" + 4);
		System.out.println("Hello" + 4);
	}
	
	void display(){
		return;	
	}
}
