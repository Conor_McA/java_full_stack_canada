
public class StringFormats {
	public static void main(String[] args) {
		String name = "Conor";
		String msg = String.format("Hey, I am %s",name);
		System.out.println(String.format("Hey, I am %s",name));
		System.out.println(msg);
		float salary = 45678.56f;
		System.out.println(String.format("My salary is: %f", salary));
		System.out.println(String.format("Value of PIE is: %1.1f", 3.1412));
		
		System.out.println(String.format("Age is %d",23));
		System.out.println(String.format("Age is %10d",23));
		System.out.println(String.format("Age is %-12d",23));
//		System.out.println(String.format("Age is %*10",345));
		
		String s1 = "Cubec";
		String s2 = new String("Cubec");
		String s3 = new String("Cubec");
		
		System.out.println(s1 == s2);//false
		System.out.println(s2 == s3);
		
		System.out.println(s2.equals(s3));//true
		s3 = s1+s2;
		System.out.println(s3);
		System.out.println(s3.equals("CUBECCUBEC"));
		System.out.println(s3.equalsIgnoreCase("CUBECCUBEC"));
//		s1.concat(s2);
//		System.out.println(s3.equals(s1+s2));
//		
//		String my_str = "Education";
//		System.out.println(my_str.substring(1,7));
//		
//		System.out.println(my_str.contains("i"));
//		
//		String result = String.join("#", "today","is","holiday");
//		System.out.println(result);
//		
//		String emptystring = "";
//		String my_emptystring = " ";
//		String your_emptystring = null;
//		System.out.println(emptystring.isEmpty());
//		System.out.println(my_emptystring.isEmpty());
////		System.out.println(your_emptystring.isEmpty());
//		String panagram = "The quick brown fox jumps over the lazy dog";
//		System.out.println(panagram);
//		String res= panagram.replace("brown", "black"); 
//		System.out.println(res);
//		
//		String h = "testing fails project";
//		String[] f = h.split("\s");
//		for(String element:f) {
//			System.out.println(element);
//		}
//	//	GOD CREATED HUMANS
//		//use split and store it in a string array
//		//reverse the array and print it
//		
//		
//		/*
//		 * 	%s - String
//		 *  %d - Integer
//		 *  %f - Float
//		 *  %e - Scientific notation
//		 *  %h - hexstring
//		 *  %o - represent octal numbers
//		 *  %t - Date/Time Conversions
//		 * */
//		String mssg = "      Welcome to Bangladesh   ";
//		System.out.println(mssg.trim());
//		System.out.println(mssg);
	}
}
