
public class NamedThreads implements Runnable{

	@Override
	public void run() {
		System.out.println("Threads in execution");
		
	}
	public static void main(String[] args) {
		NamedThreads n1 = new NamedThreads();
		NamedThreads n2 = new NamedThreads();
		Thread t1 = new Thread(n1);
		Thread t2 = new Thread(n2);
		t1.setName("Leo");
		
		System.out.println(t1.MIN_PRIORITY);
		System.out.println(t1.NORM_PRIORITY);
		System.out.println(t1.MAX_PRIORITY);
		t1.setPriority(100);
		
		System.out.println("Name of the 1st thread: (default name)"+ t1.getName());
		System.out.println("Id of the 1st thread:"+ t1.getId());
		System.out.println("Priority of the 1st thread:"+ t1.getPriority());
		t1.start();
		
		t2.setName("Aquaris");
		t2.setPriority(10);
		System.out.println("Name of the 2nd thread: (default name)"+ t2.getName());
		System.out.println("Id of the 2nd thread:"+ t2.getId());
		System.out.println("Priority of the 2nd thread:"+ t2.getPriority());
		t2.start();
		
	}

}
