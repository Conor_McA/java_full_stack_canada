
public class Vehicle {
	final double PIE = 3.141;
	void  start() {
		System.out.println("Starts method");
	}
}

// final class cannot be used for inheritance
// final methods cannot be overridden but can be accessed
// final variables cannot be reinitialized with another value but can only be accessed


// if there is a common behavior,then while overriding,the following rules must be followed

//1. The return type of the overridden method must be same everywhere
//2. The parameters(signature) must be the same everywhere
//3. The visibility (access-specifiers must be the same or higher)