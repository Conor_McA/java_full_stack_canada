import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeDemo {
	public static void main(String[] args) {
		LocalDate today = LocalDate.now();
		System.out.println(today);
		
		LocalDateTime now = LocalDateTime.now();
		System.out.println(now);
		
		DateTimeFormatter format = DateTimeFormatter.ofPattern("E, MM~dd~yyyy HH:mm:ss");
		String formattedDate = now.format(format);
		System.out.println(formattedDate);
		
	}
}
