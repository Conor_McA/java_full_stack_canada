
public class CheckData {
	public static void main(String[] args) {
		
		AgeCalculator ac = new AgeCalculator(23,344);
		ac.calculate();
		
		AgeCalculator bc = new AgeCalculator("Twenty Three",567);
		bc.calculate();
		
		AgeCalculator cc = new AgeCalculator(23.5,"SYNEEMP123");
		cc.calculate();
	}
}
