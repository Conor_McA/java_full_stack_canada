
public class Customers {
	int bankbalance = 10000;

	synchronized void withdraw(int amount) {
		System.out.println("Started the withdrawal process");
		if(this.bankbalance < amount) {
			System.out.println("Balance is less than the withdraw amt,please wait for deposit process");
			try {wait();}
			catch(InterruptedException e) {System.out.println(e);}
		}
		bankbalance -= amount;
		System.out.println("Withdrawal successful");
	}
	
	synchronized void deposit(int amount) {
		System.out.println("started the deposit process");
		bankbalance += amount;
		System.out.println("deposit process completed");
		notify();
	}
}
