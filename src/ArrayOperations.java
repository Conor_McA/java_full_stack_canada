import java.util.Scanner;

public class ArrayOperations {
	static Scanner scan = new Scanner(System.in);
	//write a method that takes array as  an input and print the array 
	// elements in the following format
	// [ele1,ele2,ele3,......,elen]

	//return array by getting input on the arraysize and the elements

	static void printArray(int []ar) {
		if (ar == null) {
			System.out.println("null");
		}
		else if(ar.length== -1) {
			System.out.println("[]");
		}
		else {
			System.out.print("[");
			for(int i=0; i<ar.length;i++) {
				System.out.print(ar[i]);
				if(i==ar.length-1) {
					System.out.print("]");
				}
				else {
					System.out.print(",");
				}
			}
		}
	}

	static int[] createArray(int size) {
		int a[] = new int[size];
		for(int i=0;i<a.length; i++) {
			System.out.println("Enter the " + (i+1)  + " element:");
			a[i] = scan.nextInt();
		}
		return a;
	}
	// Take two arrays as input and do the following

	//1. Merge two array FirstArray -> Second Array
	//2. Merge the array in zig zag fashion
	/*
	 * a[0] = 1			b[0] = 10
	 * a[1] = 2			b[1] = 12
	 * a[2] = 3			b[2] = 34
	 * a[3] = 4			b[3] = 45
	 * 					b[4] = 67
	 * result [] = {1,10,2,12,3,34,4,45,67}
	 * */
	static int[] mergeArray(int a[],int b[]) {
		int c[] = new int[a.length + b.length];
		int k=0;
		for(int i=0;i<a.length;i++) {
			c[k++] = a[i];
		}
		for(int i=0;i<b.length;i++) {
			c[k++] = b[i];
		}
		return c;
	}
	
	static int[] mergeZigZag(int a[], int b[]) {
		int c[] = new int[a.length + b.length];
		int i=0,j=0,k=0;
		while(i<a.length && j <b.length) {
			c[k++] = a[i++];
			c[k++] = b[j++];
		}
		while(i<a.length) {
			c[k++] = a[i++];
		}
		while(j <b.length) {
			c[k++] = b[j++];
		}
		return c;
	}
}
