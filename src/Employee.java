
public class Employee {
	int empid;
	String ename;
	float salary;
	Synechron sync;
	public Employee(int empid, String ename, float salary, Synechron sync) {
		this.empid = empid;
		this.ename = ename;
		this.salary = salary;
		this.sync = sync;
	}
	
	void displayEmpDetails() {
		System.out.println("Emp Id:" + this.empid);
		System.out.println("Emp Name:" + this.ename);
		System.out.println("Emp Salary:" + this.salary);
		System.out.println("Emp City:" + this.sync.city);
		System.out.println("Emp Country:" + this.sync.country);
	}
}
