import java.util.Iterator;

public class TwoDArray {
	public static void main(String[] args) {
		int k = 1;
//		int a[][] = {{1,2,3},{5,6,7},{8,7,4},{2,3,6}};
		int b[][] = new int[3][3];
		//jagged array
		int c[][] = new int[3][];
		c[0] = new int[6];
		c[1] = new int[4];
		c[2] = new int[8];
		
		
		for(int i=0;i<b.length;i++) {
			for(int j=0;j<b[i].length;j++) {
				b[i][j] = k++; 
			}
		}
		
		for(int i=0;i<b.length;i++) {
			for(int j=0;j<b[i].length;j++) {
				System.out.print(b[i][j] +" "); 
			}
			System.out.println();
		}
		
//		for(int i=0;i<c.length;i++) {
//			for(int j=0;j<c[i].length;j++) {
//				c[i][j] = k++; 
//			}
//		}
//		for(int i=0;i<c.length;i++) {
//			for(int j=0;j<c[i].length;j++) {
//				System.out.print(c[i][j] +" "); 
//			}
//			System.out.println();
//		}
		
	}
}
