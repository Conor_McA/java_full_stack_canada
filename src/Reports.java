import java.sql.SQLException;

public class Reports {
	public static void main(String[] args) {
		
		try {
		/*
		 * Scanner class object creation
		 * Connect to the DB - fetch
		 * Perform Arithemetic Calculation
		 * Connect to the DB - Update the table
		 * ##########Connection Failure ##########
		 * Fetch the report data from another table
		 */
			System.out.println(10/0);
			System.exit(0);
		 }
		catch(ArithmeticException sqle) {
			System.out.println(sqle.getMessage());
			System.exit(0);
		}
		finally {
			/*
			 * close the connectivity to DB
			 * close the scanner object
			 * */
			System.out.println("Gets executed irrespective of exception");
		}
		
	}

}
