import java.util.Scanner;

public class MenuDrivenCode {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		do {
		System.out.println("1. Cappucino");
		System.out.println("2. Americano");
		System.out.println("3. Black Tea");
		System.out.println("4. Milk");
		System.out.println("5. Espresso");
		System.out.println("6. Quit");
		System.out.println("Enter your choice [1-6]: ");
		int choice= scan.nextInt();
		switch(choice) {
		case 1:
			System.out.println("Dispense Cappucino");
			break;
		case 2:
			System.out.println("Dispense Americano");
			break;
		case 3:
			System.out.println("Dispense Black tea");
			break;
		case 4:
			System.out.println("Dispense Milk");
			break;
		case 5:
			System.out.println("Dispense Espresso");
			break;
		case 6:
			System.out.println("Thank you have a great day.");
			return;
		default:
			System.out.println("Sorry invalid choice");
		}
		}while(true);
	}
}
