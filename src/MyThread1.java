
public class MyThread1 extends Thread{
	DataTable dt;

	public MyThread1(DataTable dt){
		this.dt = dt;
	}

	@Override
	public void run() {
		dt.display(5);
	}
}

class MyThread2 extends Thread{
	DataTable dt;

	public MyThread2(DataTable dt){
		this.dt = dt;
	}

	@Override
	public void run() {
		dt.display(10);
	}
}
