
public class Gauntlet {
	private static Gauntlet g = null;
	private Gauntlet() {
		
	}
	
	public static Gauntlet getInstance() {
		if(g==null) {
			g = new Gauntlet();
			return g;
		}
		else {
			return g;
		}
	}
}
