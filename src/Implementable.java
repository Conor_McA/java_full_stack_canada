
public interface Implementable {
	@Override
	String toString();
	@Override
	int hashCode();
	
}
