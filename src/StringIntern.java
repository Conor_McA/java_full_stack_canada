
public class StringIntern {
	public static void main(String[] args) {
		String h = new String("Welcome");
		String k = "Welcome";
		
		System.out.println(h.intern()==k.intern());
		
		char c[] = "Have a break. Have a Kitkat".toCharArray();
		for(char t:c) {
			System.out.print(t+"<->");
		}
	}
}
