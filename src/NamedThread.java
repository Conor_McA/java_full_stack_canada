
public class NamedThread implements Runnable{

	@Override
	public void run() {
		System.out.println("Thread is running");
	}
	public static void main(String[] args) {
		NamedThread nt = new NamedThread();
		Thread tr= new Thread(nt,"First Thread");
		System.out.println(tr.getName());
		tr.start();
	}
}
