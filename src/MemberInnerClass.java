
public class MemberInnerClass {
// A non-static class created inside the class
// It is also called as regular inner class
// It can be declared with access specifiers like public, protected,default,private
	static int age;
	String name;
	
	static void display() {
		System.out.println(age);
	}
	void show() {
		System.out.println(name);
	}
	class InnerClass{
		static double average;
		final double PIE= 3.14;
		static void display() {
			age=23;
			System.out.println(average);
			System.out.println(age);
		}
		void show() {
			name="Connor";
			System.out.println(PIE);
			System.out.println(name);
		}
		
	}
}
