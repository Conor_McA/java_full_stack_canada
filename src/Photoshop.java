
public class Photoshop {

	void open(File file) {
		if (file instanceof JpegFile) {
			file.displaycontents();
			JpegFile j = (JpegFile)file;
			j.actions();
		}
		else if(file instanceof PngFile) {
			file.displaycontents();
			PngFile j = (PngFile)file;
			j.actions();
		}


	}



	public static void main(String[] args) {
		Photoshop ps = new Photoshop();
		JpegFile j = new JpegFile("mypic", "235 kb", ".jpeg");
		ps.open(j);
		PngFile png = new PngFile("mypic", "235 kb", ".png");
		ps.open(png);
	}
}
